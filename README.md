# README #



### What is this repository for? ###

This repository is for utils of 
GBIF (Global Biodiversity Information Facility) web services.
(http://www.gbif.org/)

### Installation ###

The preferred way to install this extension is through composer.

Either run

php composer.phar require --dev --prefer-dist endlessdreams/taxtools-gbif
or add

"endlessdreams/taxtools-gbif": "*"
to the require-dev section of your composer.json file.

### Usage ###

var_dump(GbifJsonHelper::listAllNameUsagesAcrossAllChecklistsSearch(['q'=>'Acanthaceae']));

### Who do I talk to? ###

Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>