<?php

namespace endlessdreams\taxtools\gbif\Helpers;

/**
 * Gbif Helper
 * 
 * A toolkit with to GBIF json API v1.
 * 
 * All calls allows different parameters. Here is the list of the parameters.
 * 
 * Parameter        Description
 * class            Optional class classification accepting a canonical name.
 * datasetKey       Filters by the checklist dataset key (a uuid)
 * facet            A list of facet names used to retrieve the 100 most 
 *                  frequent values for a field. Allowed facets are: datasetKey, 
 *                  higherTaxonKey, rank, status, nomenclaturalStatus, 
 *                  isExtinct, habitat, threat and nameType.
 * facetMincount    Used in combination with the facet parameter. Set 
 *                  facetMincount={#} to exclude facets with a count less than 
 *                  {#}, e.g. /search?facet=type&limit=0&facetMincount=10000 
 *                  only shows the type value 'OCCURRENCE' because 'CHECKLIST' 
 *                  and 'METADATA' have counts less than 10000.
 * facetMultiselect	Used in combination with the facet parameter. Set 
 *                  facetMultiselect=true to still return counts for values 
 *                  that are not currently filtered, 
 *                  e.g. /search?facet=type&limit=0&type=CHECKLIST&facetMultiselect=true 
 *                  still shows type values 'OCCURRENCE' and 'METADATA' even 
 *                  though type is being filtered by type=CHECKLIST
 * family           Optional family classification accepting a canonical name.
 * genus            Optional genus classification accepting a canonical name.
 * habitat          Filters by the habitat. Currently only 3 major biomes are 
 *                  accepted in our Habitat enum
 * highertaxonKey	Filters by any of the higher Linnean rank keys. Note this 
 *                  is within the respective checklist and not searching nub 
 *                  keys across all checklists.
 * hl               Set hl=true to highlight terms matching the query when 
 *                  in fulltext search fields. The highlight will be an 
 *                  emphasis tag of class 'gbifH1' 
 *                  e.g. /search?q=plant&hl=true. Fulltext search fields 
 *                  include: title, keyword, country, publishing country, 
 *                  publishing organization title, hosting organization title, 
 *                  and description. One additional full text field is 
 *                  searched which includes information from metadata 
 *                  documents, but the text of this field is not returned in 
 *                  the response.
 * isExtinct        Filters by extinction status (a boolean, e.g. 
 *                  isExtinct=true)
 * issue            A specific indexing issue as defined in our 
 *                  NameUsageIssue enum
 * kingdom          Optional kingdom classification accepting a canonical name.
 * language         Language for vernacular names. Overrides HTTP 
 *                  Accept-Language header
 * name             A scientific name which can be either a case insensitive 
 *                  filter for a canonical namestring, e.g. 'Puma concolor', 
 *                  or an input to the name parser
 * nameType         Filters by the name type as given in our NameType enum
 * nomenclaturalStatus	Not yet implemented, but will eventually allow for 
 *                  filtering by a nomenclatural status enum
 * order            Optional order classification accepting a canonical name.
 * phylum           Optional phylum classification accepting a canonical name.
 * q                Simple full text search parameter. The value for this 
 *                  parameter can be a simple word or a phrase. Wildcards are 
 *                  not supported
 * rank             Filters by taxonomic rank as given in our Rank enum
 * sourceId         Filters by the source identifier
 * status           Filters by the taxonomic status as given in our 
 *                  TaxonomicStatus enum
 * strict           If true it (fuzzy) matches only the given name, but never 
 *                  a taxon in the upper classification
 * threat           Not yet implemented, but will eventually allow for 
 *                  filtering by a threat status enum
 * verbose          If true it shows alternative matches which were considered 
 *                  but then rejected
 *                  
 * When return type is a paged array.
 * limit            Controls the number of results in the page. Using too high 
 *                  a value will be overwritten with the default maximum 
 *                  threshold, depending on the service. Sensible defaults are 
 *                  used so this may be omitted.
 * offset           Determines the offset for the search results. A limit of 
 *                  20 and offset of 20, will get the second page of 20 results.
 * 
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 * 
 * 
 *
 */
class GbifJsonHelper {

    protected static $url = 'http://api.gbif.org/v1/';
    protected static $params = [];


    /**
     * @param string $url
     * @return string
     */
    protected static function makeGetCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output,true);
    }
    
    /**
     * Lists all name usages across all checklists
     * DatasetKey is already locked to Grin dataset
     * 
     * @param array $params Available keys: language, datasetKey, sourceId, name
     * @return array NameUsage Page
     */
    public static function listAllNameUsagesAcrossAllChecklists(array $params=[]) 
    {
        $url = self::$url.'species/?'.http_build_query(self::$params + $params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Fuzzy matches scientific names against the GBIF Backbone Taxonomy with the 
     * optional classification provided. If a classification is provided and strict 
     * is not set to true, the default matching will also try to match against these 
     * if no direct match is found for the name parameter alone.
     *
     * @param array $params Available keys: rank, name, strict, verbose, kingdom, 
     *                                      phylum, class, order, family, genus
     * @return array NameUsage Page
     */
    public static function listAllNameUsagesAcrossAllChecklistsMatch(array $params=[])
    {
        $url = self::$url.'species/match?'.http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Full text search of name usages covering the scientific and vernacular name, 
     * the species description, distribution and the entire classification across 
     * all name usages of all or some checklists. Results are ordered by relevance 
     * as this search usually returns a lot of results.
     * DatasetKey is already locked to Grin dataset
     *
     * @param array $params Available keys: q, datasetKey, rank, highertaxonKey, 
     *                                      status, isExtinct, habitat, threat, 
     *                                      nameType, nomenclaturalStatus, issue, 
     *                                      hl, facet, facetMincount, facetMultiselect
     * @return array NameUsage Page
     */
    public static function listAllNameUsagesAcrossAllChecklistsSearch(array $params=[])
    {
        $url = self::$url.'species/search?'.http_build_query(self::$params + $params);
        return self::makeGetCurl($url);
    }
    
    /**
     * A quick and simple autocomplete service that returns up to 20 name usages 
     * by doing prefix matching against the scientific name. Results are ordered 
     * by relevance.
     * DatasetKey is already locked to Grin dataset
     *
     * @param array $params Available keys: q, datasetKey, rank
     * @return array NameUsage Page
     */
    public static function listAllNameUsagesAcrossAllChecklistsSuggest(array $params=[])
    {
        $url = self::$url.'species/suggest?'.http_build_query(self::$params + $params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists root usages of a checklist
     * 
     * @param string $uuidOrShortname
     * @param array $params
     * @return array NameUsage Page
     */
    public static function listRootUsagesOfAChecklist($uuidOrShortname,array $params=[])
    {
        $url = self::$url."species/root/$uuidOrShortname?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Gets the single name usage
     * 
     * @param integer $taxonKey
     * @param array $params Available keys: language
     * @return array NameUsage Page
     */
    public static function getTheSingleNameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Gets the verbatim name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array VerbatimNameUsage
     */
    public static function getTheVerbatimNameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/verbatim?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Gets the parsed name for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array ParsedName
     */
    public static function getTheParsedNameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/name?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    
    /**
     * Lists all parent usages for a name usage
     *
     * @param integer $taxonKey
     * @param array $params Available keys: language
     * @return array NameUsage List
     */
    public static function listAllParentUsagesForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/parents?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all direct child usages for a name usage
     *
     * @param integer $taxonKey
     * @param array $params Available keys: language
     * @return array NameUsage Page
     */
    public static function listAllDirectChildUsagesForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/children?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all related name usages in other checklists
     * DatasetKey is already locked to Grin dataset
     *
     * @param integer $taxonKey
     * @param array $params Available keys: language, datasetKey
     * @return array NameUsage List
     */
    public static function listAllRelatedNameUsagesInOtherChecklists($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/related?".http_build_query(self::$params + $params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all synonyms for a name usage
     *
     * @param integer $taxonKey
     * @param array $params Available keys: language
     * @return array NameUsage Page
     */
    public static function listAllSynonymsForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/synonyms?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all combinations that have this name usage as their basionym
     *
     * @param integer $taxonKey
     * @param array $params 
     * @return array NameUsage List
     */
    public static function listAllCombinationsThatHaveThisNameUsageAsTheirBasionym($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/combinations?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all descriptions for a name usage
     *
     * @param integer $taxonKey
     * @param array $params 
     * @return array NameUsage Page
     */
    public static function listAllDescriptionsForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/descriptions?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all distributions for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array NameUsage Page
     */
    public static function listAllDistributionsForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/distributions?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all media items for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array Media Page
     */
    public static function listAllMediaItemsForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/media?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all references for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array Reference Page
     */
    public static function listAllReferencesForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/references?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all species profiles for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array SpeciesProfiles Page
     */
    public static function listAllSpeciesProfilesForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/speciesProfiles?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all vernacular names for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array VernacularNames Page
     */
    public static function listAllVernacularNamesForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/vernacularNames?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all type specimens names for a name usage
     *
     * @param integer $taxonKey
     * @param array $params
     * @return array TypeSpecimens Page
     */
    public static function listAllTypeSpecimensForANameUsage($taxonKey,array $params=[])
    {
        $url = self::$url."species/$taxonKey/typeSpecimens?".http_build_query($params);
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all available enumerations
     *
     * @return array Enumeration List
     */
    public static function listAllAvailableEnumerations()
    {
        $url = self::$url."enumeration/basic";
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all available values for a given enumeration
     *
     * @param string $enumeration
     * @return array Enumeration values
     */
    public static function listAllAvailableValuesForAGivenEnumeration($enumeration)
    {
        var_dump($enumeration);
        $url = self::$url."enumeration/basic/$enumeration";
        return self::makeGetCurl($url);
    }
    
    /**
     * Lists all countries with their country code and title
     *
     * @return array Country List
     */
    public static function listAllCountriesWithTheirCountryCodeAndTitle($enumeration)
    {
        $url = self::$url."enumeration/basic/$enumeration";
        return self::makeGetCurl($url);
    }
}